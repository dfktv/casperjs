var casper = require('casper').create({
    waitTimeout: 10000,
    //stepTimeout: 10000,
    //verbose: true,
    //viewportSize: {
    //    width: 1280,
    //    height: 960
    //},
    pageSettings: {
        "userAgent": 'Mozilla/5.0 (Linux; Android 7.1.1; Nexus 6 Build/N6F26U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Mobile Safari/537.36',
        "webSecurityEnabled": false,
        "ignoreSslErrors": true
    }
});
var url = casper.cli.get('url');
var ck = casper.cli.get('ck');
var ua = casper.cli.get('ua');
if (ua != ''){
        casper.options.pageSettings.userAgent = ua;
}
casper.start();

casper.thenOpen(url, {headers:{ "Cookie" : ck }}, function(response) {
    this.echo(this.getPageContent());
});

casper.run();
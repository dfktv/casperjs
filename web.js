#!/usr/bin/env node

var express = require("express");
var exec = require('child_process').exec;
var app = express();

// adoped from Heroku's [Getting Started][] and [Spooky][]'s sample
// [Getting Started]: https://devcenter.heroku.com/articles/getting-started-with-nodejs
// [Spooky]: https://github.com/WaterfallEngineering/SpookyJS

//app.use(express.logger());
app.use(express.json());
app.use(express.urlencoded());

app.get('/', function(request, response) {
    response.send('hello');
});
app.get('/api', function(request, response) {
	var cmd = 'casperjs --ssl-protocol=any --ignore-ssl-errors=true oth.js --url='+request.query.url+' --ua=\''+request.query.ua+'\' --ck=\''+request.query.ck+'\'';
    exec(cmd, function (error, stdout) {
        if(error) console.log(error);
        response.send(stdout);
    });
});
app.post('/api', function(request, response) {
	var cmd = 'casperjs --ssl-protocol=any --ignore-ssl-errors=true oth.js --url='+request.body.url+' --ua=\''+request.body.ua+'\' --ck=\''+request.body.ck+'\'';
    exec(cmd, function (error, stdout) {
        if(error) console.log(error);
        response.send(stdout);
    });
});

var port = process.env.PORT || 5000;
app.listen(port, function() {
    console.log("Listening on " + port);
});